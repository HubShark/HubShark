<?php
// src/AppBundle/Twig/Extensions/AppExtension.php
 
namespace AppBundle\Twig\Extensions;
 
class AppExtension extends \Twig_Extension
{
//–––– OLD METHOD ––––\\
#    public function getFilters()
#    {
#        return array(
#            'created_ago' => new \Twig_Filter_Method($this, 'createdAgo'),
#        );
#    }
//––––––––| NEW METHOD |––––––––\\
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter(
            'created_ago',
            array($this, 'createdAgo')),
        );
    }

    public function createdAgo(\DateTime $dateTime)
    {
        $delta = time() - $dateTime->getTimestamp();
        if ($delta < 0)
            throw new \InvalidArgumentException("createdAgo is unable to handle dates in the future");
 
        $duration = "";
        if ($delta < 60)
        {
            // Seconds
            $time = $delta;
            $duration = $time . " second" . (($time > 1) ? "s" : "") . " ago";
        }
        else if ($delta <= 3600)
        {
            // Mins
            $time = floor($delta / 60);
            $duration = $time . " minute" . (($time > 1) ? "s" : "") . " ago";
        }
        else if ($delta <= 86400)
        {
            // Hours
            $time = floor($delta / 3600);
            $duration = $time . " hour" . (($time > 1) ? "s" : "") . " ago";
        }
        else
        {
            // Days
            $time = floor($delta / 86400);
            $duration = $time . " day" . (($time > 1) ? "s" : "") . " ago";
        }
 
        return $duration;
    }
 
    public function getName()
    {
        return 'app_extension';
    }
}
