<?php
// src/AppBundle/Controller/CommentController.php
 
namespace AppBundle\Controller;
 
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Comment;
use AppBundle\Form\CommentType;
 
/**
 * Comment controller.
 */
class CommentController extends Controller
{
    public function newAction($blog_id)
    {
        $blog = $this->getBlog($blog_id);
 
        $comment = new Comment();
        $comment->setBlog($blog);
        $form    = $this->createForm(CommentType::class);
 
        return $this->render('AppBundle:Comment:form.html.twig', array(
            'comment' => $comment,
            'form'   => $form->createView()
        ));
    }
 
    public function createAction($blog_id)
    {
        $blog = $this->getBlog($blog_id);
 
        $comment  = new Comment();
        $comment->setBlog($blog);
        $request = $this->getRequest();
        $form    = $this->createForm(CommentType::class);
        $form->bindRequest($request);
 
        if ($form->isValid()) {
            $em = $this->getDoctrine()
                       ->getManager();
            $em->persist($comment);
            $em->flush();
 
            return $this->redirect($this->generateUrl('app_blog_show', array(
                'id'    => $comment->getBlog()->getId(),
                'slug'  => $comment->getBlog()->getSlug())) .
                '#comment-' . $comment->getId()
            );
        }
 
        return $this->render('AppBundle:Comment:create.html.twig', array(
            'comment' => $comment,
            'form'    => $form->createView()
        ));
    }
 
    protected function getBlog($blog_id)
    {
        $em = $this->getDoctrine()
                    ->getManager();
 
        $blog = $em->getRepository('AppBundle:Blog')->find($blog_id);
 
        if (!$blog) {
            throw $this->createNotFoundException('Unable to find Blog post.');
        }
 
        return $blog;
    }
 
}
