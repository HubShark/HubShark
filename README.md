HubShark
=========

HubShark is a simple Multilingual CMS built using the Symfony Framework.

Wie are starting with a blog, pages, admin.


### Reqiurements

* PHP 
* MySQL Datenbank
* Apache Modul Mod Rewrite


### Functions

* Uses composer
* Symfony Framwork used
* Modern technologies used and implemented
* 


### Features

* PDO database connection
* Twig Templating
  * Twig is fast - Twig templates compile down to PHP classes so there is very little overhead to use Twig templates.
  * Twig is concise - Twig allows us to perform templating functionality in very little code. Compare this to PHP where some statements become very verbose.
  * Twig supports template inheritance - This is one of my personal favorites. Templates have the ability to extend and override other templates allowing children templates to change the defaults provided by their parents.
  * Twig is secure - Twig has output escaping enabled by default and even provides a sand boxed environment for imported templates.
  * Twig is extensible - Twig comes will a lot of common core functionality that you’d expected from a templating engine, but for those occasions where you need some extra bespoke functionality, Twig can be easily extended.



### Documentation


### License

HubShark is released under the MIT License

